//
//  customCell.swift
//  weatherNowApp
//
//  Created by Jo on 1/11/19.
//  Copyright © 2019 Aleksandr Duma. All rights reserved.
//

import UIKit



class TableCustomCell: UITableViewCell {
    
    
    var dateTime: Date?
    var temp: Double?
    
    var dateTimeView: UITextView = {
        var textView = UITextView()
        textView.translatesAutoresizingMaskIntoConstraints = false
        return textView
    }()
    
    var tempView: UITextView = {
        var tempView = UITextView()
        tempView.translatesAutoresizingMaskIntoConstraints = false
        return tempView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.addSubview(dateTimeView)
        self.addSubview(tempView)
        
        dateTimeView.isUserInteractionEnabled = false
        dateTimeView.font = .systemFont(ofSize: 22)
        dateTimeView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        dateTimeView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        dateTimeView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        dateTimeView.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.5).isActive = true
        
        tempView.isUserInteractionEnabled = false
        tempView.font = .systemFont(ofSize: 22)
        tempView.textAlignment = .right
        tempView.leftAnchor.constraint(equalTo: self.dateTimeView.rightAnchor).isActive = true
        tempView.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        tempView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        tempView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        tempView.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.5).isActive = true
        
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        if let dateTime = dateTime{
            dateTimeView.text = dateTime.toString(dateFormat: "HH:mm")
            
        }
        if let temp = temp{
            tempView.text = String(format: "%.1f ºC", temp)
            
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

