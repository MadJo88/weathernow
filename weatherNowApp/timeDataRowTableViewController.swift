//
//  timeDateRowTableViewController.swift
//  weatherNowApp
//
//  Created by Jo on 1/8/19.
//  Copyright © 2019 Aleksandr Duma. All rights reserved.
//

import UIKit

class timeDataRowTableViewController: UITableViewController {
    //MARK: Properties
    
    var rows = [timeDataRow]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadSampleTimeDates()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rows.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "timeDataTableCell"
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? timeDataTableCell  else {
            fatalError("The dequeued cell is not an instance of MealTableViewCell.")
        }
        // Fetches the appropriate time-data for the data source layout.
        let row = rows[indexPath.row]
        cell.time.text = row.time
        cell.temp.text = row.temp

        return cell
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    private func loadSampleTimeDates() {
        guard let timeData1 = timeDataRow(time: "11:00", temp: "22 C") else {
            fatalError("Unable to instantiate timeDate1")
        }
        
        guard let timeData2 = timeDataRow(time: "12:00", temp: "23 C") else {
            fatalError("Unable to instantiate timeDate2")
        }
        
        guard let timeData3 = timeDataRow(time: "13:00", temp: "24 C") else {
            fatalError("Unable to instantiate timeDate3")
        }
        rows += [timeData1, timeData2, timeData3]
    }
}
