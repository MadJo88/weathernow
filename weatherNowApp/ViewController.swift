//
//  ViewController.swift
//  weatherNowApp
//
//  Created by Jo on 1/6/19.
//  Copyright © 2019 Aleksandr Duma. All rights reserved.
//

import UIKit

struct TableCellData{
    var dateTime: Date?
    var temp: Double?
}

extension Date{
    func toString( dateFormat format  : String ) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
}

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource  {
    // MARK: - Properties
    // Data model: These strings will be the data for the table view cells
    // cell reuse id (cells that scroll out of view can be reused)
    let cellReuseIdentifier = "customCell"
    let collectionCellReuseIdentifier = "collectionCustomCell"
    // MARK: - Outlets

    @IBOutlet var tableView: UITableView!
    @IBOutlet var collectionView: UICollectionView!
    
    // - Constants
    private let locationManager = LocationManager()
    
    
    // MARK: - View methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Register the table view cell class
        self.tableView.register(TableCustomCell.self, forCellReuseIdentifier: cellReuseIdentifier)
        self.collectionView.register(CollectionCustomCell.self, forCellWithReuseIdentifier: collectionCellReuseIdentifier)
        // (optional) include this line if you want to remove the extra empty cell divider lines
        self.tableView.tableFooterView = UIView()

        NotificationCenter.default.addObserver(self, selector: #selector(refreshTableAndCollection(notification:)), name: Notification.Name("dataChanged"), object: nil)
        
        
        ///
        showPlaceInConsole()
    }
    func showPlaceInConsole(){
        guard let exposedLocation = self.locationManager.exposedLocation else {
            print("*** Error in \(#function): exposedLocation is nil")
            return
        }
        self.locationManager.getPlace(for: exposedLocation) { placemark in
            guard let placemark = placemark else { return }
            
            var output = "Our location is:"
            if let country = placemark.country {
                output = output + "\n\(country)"
            }
            if let state = placemark.administrativeArea {
                output = output + "\n\(state)"
            }
            if let town = placemark.locality {
                output = output + "\n\(town)"
            }
            print(output)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        showPlaceInConsole()
        self.locationManager.requestLocationManual()
        //ServerLogic.serverApiResponce.getWeather()
    }
    override func viewDidAppear(_ animated: Bool) {
        //createAlert(alertTitle: "Notice")
    }
    // MARK: - General purpose methods
    func createAlert (alertTitle: String){
        // create the alert
        let alert = UIAlertController(title: alertTitle, message: "Lauching this missile will destroy the entire universe. Is this what you intended to do?", preferredStyle: UIAlertController.Style.alert)
        
        // add the actions (buttons)
        alert.addAction(UIAlertAction(title: "Remind Me Tomorrow", style: UIAlertAction.Style.default, handler: nil))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Launch the Missile", style: UIAlertAction.Style.destructive, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func refreshTableAndCollection(notification: NSNotification) {
        self.tableView.reloadData()
        self.collectionView.reloadData()
    }
    
    // MARK: - Table methods
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ServerLogic.serverApiResponce.tableViewDataArray.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // create a new cell if needed or reuse an old one
        let cell:TableCustomCell = self.tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! TableCustomCell
        cell.dateTime = ServerLogic.serverApiResponce.tableViewDataArray[indexPath.row].dateTime
        cell.temp = ServerLogic.serverApiResponce.tableViewDataArray[indexPath.row].temp
        
        return cell
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
    }
    
    // MARK: - Collection methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ServerLogic.serverApiResponce.collectionViewDataArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let collectionCell:CollectionCustomCell = self.collectionView.dequeueReusableCell(withReuseIdentifier: collectionCellReuseIdentifier, for: indexPath) as! CollectionCustomCell
        //let dateLabel = collectionCell.viewWithTag(33) as! UILabel
        collectionCell.date = ServerLogic.serverApiResponce.collectionViewDataArray[indexPath.row]
        
        return collectionCell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        ServerLogic.serverApiResponce.tableViewDataArray = ServerLogic.serverApiResponce.createDataForTableView(dateForSearch: Calendar.current.component(.day, from: ServerLogic.serverApiResponce.collectionViewDataArray[indexPath.row]), weatherArray: ServerLogic.serverApiResponce.cellDataArray)
        
        self.tableView.reloadData()
    }
}

