//
//  ServerLogic.swift
//  weatherNowApp
//
//  Created by Jo on 1/21/19.
//  Copyright © 2019 Aleksandr Duma. All rights reserved.
//

import Foundation
import CoreLocation

//openweathermap user ID, free version
let apiID = "f369712b5928ea3e1ef1931c4b37a0ce"
//city ID, get from openweathermap
let cityID = 706483


//URL for api connection
//let weatherURL = URL(string: "http://api.openweathermap.org/data/2.5/forecast?id=\(cityID)&units=metric&APPID=\(apiID)")!


class ServerLogic{
    // MARK: - Properties
    
    //Singleton obj
    static let serverApiResponce = ServerLogic ()
    
    //weather data array after serealization and parsing
    var cellDataArray = [TableCellData]()
    //data array for table veiw
    var tableViewDataArray = [TableCellData]()
    //data array for collection veiw
    var collectionViewDataArray = [Date]()
    //current location coordinates
    var locationLat: CLLocationDegrees?
    var locationLon: CLLocationDegrees?
    // MARK: - Methods
    
    func getWeather() {
        let weatherURL = URL(string: "http://api.openweathermap.org/data/2.5/forecast?lat=\(self.locationLat ?? 0)&lon=\(self.locationLon ?? 0)&units=metric&APPID=\(apiID)")!
        let session = URLSession.shared
        let dataTask = session.dataTask(with: weatherURL) {
            (data: Data?, response: URLResponse?, error: Error?) in
            if let error = error {
                print("Error:\n\(error)")
            } else {
                if let data = data {
                    if let jsonObj = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? NSDictionary {
                        if let mainArray = jsonObj!.value(forKey: "list") as? NSArray {
                            
                            for dict in mainArray {
                                //parse for dateTime
                                if let dateTime = (dict as! NSDictionary).value(forKey: "dt") as? TimeInterval {
                                    let dateNow = Date (timeIntervalSince1970: dateTime)
                                    ServerLogic.serverApiResponce.cellDataArray.append(TableCellData(dateTime: dateNow, temp: 0))
                                } else {
                                    print("Error: unable to find date and time in dictionary")
                                }
                                //parse for temperature
                                if let mainDictionary = (dict as! NSDictionary).value(forKey: "main") as? NSDictionary {
                                    if let temperature = mainDictionary.value(forKey: "temp") as? Double {
                                        ServerLogic.serverApiResponce.cellDataArray[ServerLogic.serverApiResponce.cellDataArray.count - 1].temp = temperature
                                    } else {
                                        print("Error: unable to find temperture in dictionary")
                                    }
                                } else {
                                    print("Error: unable to find main dictionary")
                                }
                            }
                            DispatchQueue.main.async {
                                ServerLogic.serverApiResponce.tableViewDataArray = ServerLogic.serverApiResponce.createDataForTableView(dateForSearch: Calendar.current.component(.day, from: ServerLogic.serverApiResponce.cellDataArray[0].dateTime!), weatherArray: ServerLogic.serverApiResponce.cellDataArray)
                                ServerLogic.serverApiResponce.collectionViewDataArray = ServerLogic.serverApiResponce.createDataForCollectionView(weatherArray: ServerLogic.serverApiResponce.cellDataArray)
                                
                                NotificationCenter.default.post(name: Notification.Name("dataChanged"), object: nil)
                            }
                        }
                    } else {
                        print("Error: unable to convert json data")
                    }
                } else {
                    print("Error: did not receive data")
                }
            }
        }
        dataTask.resume()
    }
    
    func createDataForTableView(dateForSearch: Int, weatherArray: Array<TableCellData>) -> Array<TableCellData>{
        var tableViewDataArray = [TableCellData]()
        for dates in weatherArray where Calendar.current.component(.day, from: dates.dateTime!) == dateForSearch{
            tableViewDataArray.append(dates)
        }
        return tableViewDataArray
    }
    
    func createDataForCollectionView(weatherArray: Array<TableCellData>) -> Array<Date>{
        collectionViewDataArray = [Date]()
        for dates in weatherArray{
            if collectionViewDataArray.contains(Calendar.current.startOfDay(for: dates.dateTime!)) == false{
                collectionViewDataArray.append(Calendar.current.startOfDay(for: dates.dateTime!))
            }
        }
        return collectionViewDataArray
    }
}
