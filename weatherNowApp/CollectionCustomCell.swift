//
//  customCell.swift
//  weatherNowApp
//
//  Created by Jo on 1/11/19.
//  Copyright © 2019 Aleksandr Duma. All rights reserved.
//

import UIKit


class CollectionCustomCell: UICollectionViewCell{
    
    
    var date: Date?
    
    var dateView: UITextView = {
        var textView = UITextView()
        textView.translatesAutoresizingMaskIntoConstraints = false
        return textView
    }()
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if let date = date{
            dateView.text = date.toString(dateFormat: "dd.MM.yy")
        }
        
        dateView.frame = super.bounds
        dateView.textAlignment = .center
        
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(dateView)
        
        
        dateView.isUserInteractionEnabled = false
        dateView.font = .systemFont(ofSize: 30)
        dateView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        dateView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        dateView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

