//
//  Meal.swift
//  weatherNowApp
//
//  Created by Jo on 1/8/19.
//  Copyright © 2019 Aleksandr Duma. All rights reserved.
//

import UIKit

class timeDataRow {
    let time: String
    let temp: String
    
    init!(time: String, temp: String) {
        self.time = time
        self.temp = temp
    }
}
